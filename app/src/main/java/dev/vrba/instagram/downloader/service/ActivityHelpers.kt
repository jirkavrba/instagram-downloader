package dev.vrba.instagram.downloader.service

import android.app.Activity
import android.widget.Toast
import dev.vrba.instagram.downloader.R

object ActivityHelpers {

    fun openInstagram(context: Activity) {
        val intent = context.packageManager.getLaunchIntentForPackage("com.instagram.android")

        if (intent == null) {
            Toast.makeText(context, R.string.instagram_app_not_found, Toast.LENGTH_SHORT).show()
            return
        }

        context.finish()
        context.startActivity(intent)
    }
}