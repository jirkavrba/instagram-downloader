package dev.vrba.instagram.downloader

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.lifecycleScope
import dev.vrba.instagram.downloader.exception.PrivateContentException
import dev.vrba.instagram.downloader.exception.RateLimitedException
import dev.vrba.instagram.downloader.service.ActivityHelpers.openInstagram
import dev.vrba.instagram.downloader.service.InstagramDownloadService
import dev.vrba.instagram.downloader.ui.theme.InstagramDownloaderTheme
import kotlinx.coroutines.launch
import java.util.UUID

class DownloadActivity : ComponentActivity() {

    private val tag: String = "InstagramDownloader::MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (intent.action == Intent.ACTION_SEND && intent.type == "text/plain") {
            downloadInstagramLink(intent.getStringExtra(Intent.EXTRA_TEXT))
        }

        setContent {
            InstagramDownloaderTheme {
                Surface(
                    color = MaterialTheme.colorScheme.background,
                ) {
                    Column(
                        modifier = Modifier.padding(all = 20.dp),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.spacedBy(
                            20.dp,
                            Alignment.CenterVertically
                        ),
                    ) {
                        Row(
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.spacedBy(
                                20.dp,
                                Alignment.CenterHorizontally
                            )
                        ) {
                            CircularProgressIndicator()
                            Text(
                                stringResource(id = R.string.downloading),
                                style = MaterialTheme.typography.titleMedium
                            )
                        }
                    }
                }
            }
        }
    }

    private fun downloadInstagramLink(link: String?) {
        if (link == null) {
            Toast.makeText(this, R.string.instagram_link_is_empty, Toast.LENGTH_LONG).show()
            return
        }

        lifecycleScope.launch {
            val context = this@DownloadActivity

            Log.i(tag, "Downloading link: $link")
            Toast.makeText(context, R.string.downloading_content, Toast.LENGTH_SHORT).show()

            try {
                val result = InstagramDownloadService.getMediaUrl(link)

                if (result == null) {
                    Toast.makeText(context, R.string.unsupported_link_format, Toast.LENGTH_SHORT)
                        .show()
                    return@launch
                }

                // TODO: This if fucking ugly
                val extension = result.split("?").first().split(".").last()
                val filename = "${UUID.randomUUID()}.$extension"

                Log.i(tag, "Media url: $result")
                Log.i(tag, "Generated filename: $filename")

                val manager = context.getSystemService(DownloadManager::class.java)
                val request = DownloadManager.Request(Uri.parse(result))
                    .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
                    .setTitle(filename)
                    .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename)
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)

                val receiver = object : BroadcastReceiver() {
                    override fun onReceive(context: Context?, intent: Intent?) {
                        Log.i(tag, "Download completed!")
                        Toast.makeText(context, R.string.downloading_complete, Toast.LENGTH_SHORT)
                            .show()
                        openInstagram(this@DownloadActivity)
                    }
                }

                context.registerReceiver(receiver, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
                manager.enqueue(request)
            }
            catch (exception: PrivateContentException) {
                Toast.makeText(context, R.string.private_content, Toast.LENGTH_LONG).show()
            }
            catch (exception: RateLimitedException) {
                Toast.makeText(context, R.string.rate_limited, Toast.LENGTH_LONG).show()
            }
        }
    }
}