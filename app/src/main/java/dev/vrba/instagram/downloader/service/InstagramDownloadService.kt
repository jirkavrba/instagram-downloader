package dev.vrba.instagram.downloader.service

import android.util.Log
import dev.vrba.instagram.downloader.exception.PrivateContentException
import dev.vrba.instagram.downloader.exception.RateLimitedException
import dev.vrba.instagram.downloader.model.InstagramApiResponse
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.cio.CIO
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.bodyAsText
import io.ktor.http.HttpStatusCode
import io.ktor.serialization.jackson.*


object InstagramDownloadService {

    private const val tag: String = "InstagramDownloader::InstagramDownloadService"

    private val client = HttpClient(CIO) {
        install(ContentNegotiation) {
            jackson()
        }
    }

    suspend fun getMediaUrl(link: String): String? {
        val cleaned = if (link.contains("?igshid=")) link.split("?igshid")[0] else link

        Log.i(tag, "Obtaining media url for link: $link")
        Log.i(tag, "Cleaned url without tracking: $cleaned")

        return downloadMedia(cleaned)
    }

    private suspend fun downloadMedia(link: String): String? {
        val response = client.get("$link?__a=1&__d=dis")

        Log.i(tag, "Received status code: ${response.status}")

        return when (response.status) {
            HttpStatusCode.OK -> parseMediaUrl(link, response)
            HttpStatusCode.NotFound -> throw PrivateContentException
            HttpStatusCode.TooManyRequests -> throw RateLimitedException
            else -> null
        }
    }

    private suspend fun parseMediaUrl(link: String, response: HttpResponse): String? {
        Log.i(tag, "Parsing media url for $link")
        Log.i(tag, "Received response: ${response.bodyAsText()}")

        val body = response.body<InstagramApiResponse>()

        Log.i(tag, "Received GraphQL response: $body")

        return when (body.graphql.media.type) {
            "GraphImage" -> body.graphql.media.image
            "GraphVideo" -> body.graphql.media.video
            else -> null
        }
    }
}