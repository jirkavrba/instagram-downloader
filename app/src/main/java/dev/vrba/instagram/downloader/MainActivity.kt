package dev.vrba.instagram.downloader

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import dev.vrba.instagram.downloader.service.ActivityHelpers.openInstagram
import dev.vrba.instagram.downloader.ui.theme.InstagramDownloaderTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            InstagramDownloaderTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Column(
                        modifier = Modifier.padding(all = 20.dp),
                        verticalArrangement = Arrangement.spacedBy(
                            Dp(20.0F),
                            Alignment.CenterVertically
                        ),
                        horizontalAlignment = Alignment.CenterHorizontally,
                    ) {
                        Text(
                            text = stringResource(id = R.string.app_title),
                            textAlign = TextAlign.Center,
                            style = MaterialTheme.typography.displaySmall
                        )
                        Text(
                            text = stringResource(id = R.string.app_subtitle),
                            textAlign = TextAlign.Center
                        )
                        Text(
                            text = stringResource(id = R.string.instructions),
                            textAlign = TextAlign.Center,
                            style = MaterialTheme.typography.bodyMedium
                        )
                        Button(onClick = { openInstagram(this@MainActivity) }) {
                            Text(
                                text = stringResource(id = R.string.open_instagram)
                            )
                        }
                    }
                }
            }
        }
    }

}