package dev.vrba.instagram.downloader.exception

object PrivateContentException : Exception("The content of this post/reel is marked as private!")