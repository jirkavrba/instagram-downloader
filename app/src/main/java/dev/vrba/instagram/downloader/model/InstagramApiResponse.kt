package dev.vrba.instagram.downloader.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class ShortCodeMedia(
    @JsonProperty("__typename")
    val type: String,

    @JsonProperty("display_url", required = false)
    val image: String?,

    @JsonProperty("video_url", required = false)
    val video: String?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class GraphqlResponse(
    @JsonProperty("shortcode_media")
    val media: ShortCodeMedia
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class InstagramApiResponse(
    @JsonProperty("graphql")
    val graphql: GraphqlResponse
)