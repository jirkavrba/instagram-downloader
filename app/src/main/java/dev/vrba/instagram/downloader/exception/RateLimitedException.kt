package dev.vrba.instagram.downloader.exception

object RateLimitedException : Exception("Rate limited by the Instagram API")